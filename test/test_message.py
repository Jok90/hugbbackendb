import unittest
from datetime import datetime
from app.data import dummydata
from app.services.message_service import MessageService
from app.models.Message import Message

""" Tests the backend's chat/messaging functionality """
class TestMessage(unittest.TestCase):
    """ A method that is run before every test """
    def setUp(self):
        self.__message_service = MessageService()

    """ Tests whether send_message stores the new message correctly """
    def test_send_message_stores_new_message(self):
        new_message = \
            Message(
                2,
                3,
                str(datetime.now()),
                "According to all known laws of aviation"
            )
        self.__message_service.send_message(new_message)
        message = dummydata.messages[2][-1]
        self.assertEqual(new_message.get_message(), message.get_message())
        self.assertEqual(new_message.get_sender_id(), message.get_sender_id())
        self.assertEqual(new_message.get_receiver_id(), message.get_receiver_id())
        self.assertEqual(new_message.get_timestamp(), message.get_timestamp())

    """
        Tests if send_message raises a TypeError if the message object given is
        not an instance of the Message class.
    """
    def test_send_message_raises_type_error_if_not_message(self):
        new_message = None
        with self.assertRaises(TypeError):
            self.__message_service.send_message(new_message)
    
    """
        Test if get_messages_to_user_id returns all the messages sent to the
        user with the specified ID.
    """
    def test_get_messages_to_user_id_returns_all_messages(self):
        message = self.__message_service.get_messages_to(1)[0]
        self.assertEqual(message.get_sender_id(), 2)
        self.assertEqual(message.get_receiver_id(), 1)
        self.assertEqual(message.get_timestamp(), "2019-09-17 22:02:17")
        self.assertEqual(message.get_message(), "I'm interested")

    """
        Test if get_messages_by_user_id returns all the messages written by the
        user with the given user ID.
    """
    def test_get_messages_by_user_id_returns_all_messages(self):
        messages = self.__message_service.get_messages_by(1)
        for index, message in enumerate(dummydata.messages[1]):
            self.assertEqual(message.get_sender_id(), messages[index].get_sender_id())
            self.assertEqual(message.get_receiver_id(), messages[index].get_receiver_id())
            self.assertEqual(message.get_timestamp(), messages[index].get_timestamp())
            self.assertEqual(message.get_message(), messages[index].get_message())
    
    """
        Test if get_messages_from_a_to_b returns all the messages written by the
        user with ID=a sent to user with ID=b
    """
    def test_get_messages_from_a_to_b_returns_all_messages(self):
        messages = self.__message_service.get_messages_from_a_to_b(2, 1)
        for message in messages:
            self.assertEqual(message.get_sender_id(), 2)
            self.assertEqual(message.get_receiver_id(), 1)


if __name__ == "__main__":
    unittest.main()