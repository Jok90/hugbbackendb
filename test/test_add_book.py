import app.data.dummydata as dummydata
import unittest
from app.services.book_service import BookService
import app.models.Book as modelbook

""" Tests the add_book() function """
class testAddBook(unittest.TestCase):
    """ Sets up the test for each test method"""
    def setUp(self):
        self.__bookservice = BookService()
        self.booklist = self.__bookservice.get_all_books()
        self.testbook = modelbook.Book

    """ Cleans up for the next test method """
    def tearDown(self):
        self.booklist = None
        self.book = None

    """ Tests if adding a valid book works correctly """
    def test_add_book(self):
        self.book = self.testbook(4, 2, 'Bible', 'God', 'Fiction', 0, 'Bad', 'Available')
        numbbooks = len(self.booklist)
        self.booklist = self.__bookservice.add_book(self.book)
        self.assertEqual(numbbooks+1, len(self.__bookservice.get_all_books()))

    """ Tests adding a book with a missing name """
    def test_add_book_missing_input(self):
        self.book = self.testbook(4, 2, '', 'God', 'Fiction', 0, 'Bad', 'Available')
        self.assertEqual(self.__bookservice.add_book(self.book), "Some required information was not given")

    """ Tests adding a book that is equal to None """
    def test_add_book_no_book(self):
        self.book = None
        self.assertEqual(self.__bookservice.add_book(self.book), "Some required information was not given")


if __name__ == "__main__":
    unittest.main()