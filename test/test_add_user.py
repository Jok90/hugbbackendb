import app.data.dummydata as dummydata
from app.services import user_service
from app.models import User
import unittest


""" Tests the add_user() function """
class TestAddUser(unittest.TestCase):
    """ Sets up the test for each test method """
    def setUp(self):
        self.user4 = User.User("John Johnsson", "jon@jonsson.com", "jonsson", "yomama")
        self.__user_service = user_service.UserService()

    """ Cleans up for the next test method """
    def tearDown(self):
        dummydata.users = {1: dummydata.user1, 2: dummydata.user2, 3: dummydata.user3}
        dummydata.next_user_id = 4

    """ Tests adding a valid user """
    def test_add_user_success(self):
        self.__user_service.add_user(self.user4)
        self.assertEqual(dummydata.users, {1: dummydata.user1, 2: dummydata.user2, 3: dummydata.user3, 4: self.__user_service.get_user(4)})
        self.assertTrue(self.user4.get_id() == 4)

    """ Tests if the automatic id system is working correctly """
    def test_add_userid_increment(self):
        self.__user_service.add_user(self.user4)
        self.assertTrue(self.__user_service.get_user(4).get_id() == 4)

    """ Tests adding a user when given an invalid email """
    def test_add_user_invalid_email(self):
        self.user4.set_email("someinvalidemail")
        self.assertEqual(self.__user_service.add_user(self.user4), "Invalid email!")

    """ Tests adding a user that has the same email as an already existing user """
    def test_add_user_email_exists(self):
        self.user4.set_email("user2@user.is")
        self.assertEqual(self.__user_service.add_user(self.user4), "Email already in use!")

    """ Tests adding a user that has a missing attribute """
    def test_add_user_missing_attribute(self):
        new_user = self.__create_bad_user4()
        self.assertEqual(self.__user_service.add_user(new_user), "Missing attribute!")
    
    """ Helper function that creates a invalid user """
    def __create_bad_user4(self):
        new_user = User.User("Þór Breki", "thorbreki@gmail.com", None, "yomama")
        return new_user
        
if __name__ == "__main__":
       unittest.main()