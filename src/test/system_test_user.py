import unittest
import asyncio
import json
from app.client import client

class TestSystemUser(unittest.TestCase):
    def setUp(self):
        dummy_user = {
            "name": "Grognak the Destroyer",
            "email": "attorney@law.com",
            "username": "Amnozia",
            "password": "IForgot",
            "is_admin": False
        }
        body = {
            "op": "add_user",
            "user_model": dummy_user
        }
        result = asyncio.get_event_loop() \
                        .run_until_complete(client.send_test_message(body))
        self.__dummy_user = json.loads(result)
    
    """ Method that is run after every test case """
    def tearDown(self):
        body = { "op": "hard_delete_user", "user_id": int(self.__dummy_user["id"]) }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        body = { "op": "decrement_next_user_id" }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
    
    """ Tests if add_user method returns correct user model. """
    def test_add_user_returns_user_model(self):
        self.assertEqual(type(self.__dummy_user), dict)
        self.assertTrue("id" in self.__dummy_user)
        self.assertTrue("name" in self.__dummy_user)
        self.assertTrue("email" in self.__dummy_user)
        self.assertTrue("username" in self.__dummy_user)
        self.assertTrue("password" in self.__dummy_user)
        self.assertTrue("is_admin" in self.__dummy_user)
    
    """ Tests if recover_user method marks user correctly. """
    def test_recover_user_marks_user_as_recovered(self):
        # Try recovering dummy user
        body = {
            "op": "recover_user",
            "user_id": self.__dummy_user["id"]
        }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        
        # See if its status == 1
        body = {
            "op": "get_user_info",
            "user_id": self.__dummy_user["id"]
        }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        user_model = json.loads(return_val)
        self.assertEqual(user_model["status"], 1)
    
    """ Tests if remove_user method marks user correctly. """
    def test_remove_user_marks_user_as_removed(self):
        # Try removing dummy user
        body = {
            "op": "remove_user",
            "user_id": self.__dummy_user["id"]
        }
        asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        
        # See if its status == 2
        body = {
            "op": "get_user_info",
            "user_id": self.__dummy_user["id"]
        }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        user_model = json.loads(return_val)
        self.assertEqual(user_model["status"], 2)

    """ Tests if get_user_info method returns correct user info. """
    def test_get_user_info_returns_correct_user(self):
        # Get info from the user
        body = {
            "op": "get_user_info",
            "user_id": self.__dummy_user["id"]
        }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        user_info = json.loads(return_val)

        # Check if info matches
        self.assertEqual(user_info["id"], self.__dummy_user["id"])
        self.assertEqual(user_info["name"], self.__dummy_user["name"])
        self.assertEqual(user_info["email"], self.__dummy_user["email"])
        self.assertEqual(user_info["username"], self.__dummy_user["username"])
        self.assertEqual(user_info["password"], self.__dummy_user["password"])
        self.assertEqual(user_info["is_admin"], self.__dummy_user["is_admin"])
    
    """ Tests if get_all_users method returns some users. """
    def test_get_all_users_returns_non_empty_dict(self):
        body = { "op": "get_all_users" }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        all_users = json.loads(return_val)
        self.assertTrue(type(all_users) == dict)
        self.assertTrue(len(all_users) > 0)

    """ Tests if filter_user returns a correct collection. """
    def test_filter_users(self):
        body = { "op": "filter_users",
                 "search_model": {
                     "name": "Don't delete me 1",
                     "email": "dontdelete1@gmail.com",
                     "username": "dont.delete.1"
                 }
            }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        all_users = json.loads(return_val)

        for user in all_users.values():
            self.assertEqual(user["name"], "Don't delete me 1")
            self.assertEqual(user["email"], "dontdelete1@gmail.com")
            self.assertEqual(user["username"], "dont.delete.1")
    
    """ Tests if log_in method logs the user in. """
    def test_log_in_users(self):
        body = { "op": "log_in_user",
                 "user_id": -1
            }
        return_val = asyncio.get_event_loop() \
            .run_until_complete(client.send_test_message(body))
        the_string = return_val
        self.assertEqual(type(the_string), str)
        self.assertEqual(len(the_string), 30)


if __name__ == "__main__":
    unittest.main()