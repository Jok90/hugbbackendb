import json
from pathlib import Path

""" A class for encapsulating the functionality of the exchange repository """
class ExchangeRepository(object):
    """ Gets all the exchanges in the data base """
    def get_all_exchanges(self):
        with open(self.__get_path(), 'r') as rf:
            rentaldata = json.loads(rf.read())
        return rentaldata

    """ Returns an exchange with the given ID"""
    def get_exchange(self, exchange_id):
        all_exchanges = self.get_all_exchanges()
        return all_exchanges.get(str(exchange_id))

    """ Reads and returns the next available ID for a new exchange object """
    def get_next_exchange_id(self):
        all_ids = {}
        with open("app/data/ids.json", "r") as filestream:
            all_ids = json.loads(filestream.read())
        return all_ids["next_exchange_id"]
    
    """ Adds a new exchange object to the data base """
    def add_exchange(self, exchange_dict):
        all_exchanges = self.get_all_exchanges()
        exchange_id = str(exchange_dict["id"])
        all_exchanges[exchange_id] = exchange_dict
        with open("app/data/exchangedata.json", "w") as filestream:
            filestream.write(json.dumps(all_exchanges))
        self.increment_next_exchange_id()
        return exchange_dict

    """ Sets an exchange from the file with the given ID to removed """
    def delete_exchange(self, exchange_id):
        all_exchanges = self.get_all_exchanges()
        if str(exchange_id) not in all_exchanges:
            raise ValueError(f"Trying to remove a book with ID={exchange_id} that doesn't exist")
        all_exchanges[str(exchange_id)]["status"] = 3
        with open("app/data/exchangedata.json", "w") as filestream:
            filestream.write(json.dumps(all_exchanges))
    """ Deletes an exchange with the given ID from the database """
    def hard_delete_exchange(self, exchange_id):
        all_exchanges = {}
        with open("app/data/exchangedata.json", "r") as filestream:
            all_exchanges = json.loads(filestream.read())
        try:
            del all_exchanges[str(exchange_id)]
        except KeyError:
            pass
        with open("app/data/exchangedata.json", "w") as filestream:
            filestream.write(json.dumps(all_exchanges))
    
    """ Increments next exchange ID """
    def increment_next_exchange_id(self):
        all_ids = {}
        with open("app/data/ids.json", "r") as filestream:
            all_ids = json.loads(filestream.read())
        all_ids["next_exchange_id"] += 1
        with open("app/data/ids.json", "w") as filestream:
            filestream.write(json.dumps(all_ids))

    """ Decrements next exchange ID (For testing purpose only)"""
    def decrement_next_exchange_id(self):
        all_ids = {}
        with open("app/data/ids.json", "r") as filestream:
            all_ids = json.loads(filestream.read())
        all_ids["next_exchange_id"] -= 1
        with open("app/data/ids.json", "w") as filestream:
            filestream.write(json.dumps(all_ids))

    """ Returns the reletive path to json file"""
    def __get_path(self):
        base_path = Path(__file__).parent
        return (base_path / "../data/exchangedata.json").resolve()